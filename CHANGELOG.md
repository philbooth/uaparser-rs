# Change log

## 2.0.1

### Bug fixes

* deps: downgrade fancy-regex (50e0eb0)

## 2.0.0

### Breaking changes

* deps: rustup update + cargo update (4d94293)

### Other changes

* package: add badges to package metadata (4d8cc55)

## 1.0.0

### Breaking changes

* api: propagate initialisation errors from init (cd11fe4)

### New features

* api: distinguish parse errors from initialisation errors (d80eb59)

### Other changes

* tests: add some more test coverage (bcb42b2)
* docs: add usage comment for the init function (b6e96d5)

## 0.1.2

### Other changes

* deps: use a published fork of fancy-regex (296dcd3)

## 0.1.1

### Other changes

* project: switch dev environment to stable (fe26b1a)

## 0.1.0

### New features

* docs: add readme and changelog files (29891dd)
* docs: improve the doc comments (5c4a14a)
* perf: expose init function for early initialisation (9075986)
* tests: write basic passing unit tests (2457b6c)

### Bug fixes

* api: propagate errors from parsing methods (319d243)
* parsing: use correct capture index for device model (64679f4)
* parsing: map empty strings to None (efb4c62)
* lib: fix compiler errors in initial implementation (adc286d)
* build: fix compiler errors in generated code (46d20c4)
* deps: use RegexBuilder::size_limit (a5c9225)

### Refactorings

* lib: pull some repetitive boilerplate into macros (beb0dd6)
* deps: replace regex with fancy-regex (bdd4946)

### Other changes

* docs: add contribution guide (8a20874)
* project: add ci config (30b6ff0)
* code: changed pinned toolchain, run rustfmt (c8b8a0f)

## 0.0.0

### Features

* lib: initial implementation (fce06d6)
