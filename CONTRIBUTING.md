# Contribution guidelines

* Make sure all of the tests pass:

  ```
  cargo t
  ```

* New code must have new tests.

* Format the code:

  ```
  cargo fmt
  ```

* Adhere to the conventions
  used elsewhere in the codebase.

* Update the [readme file](README.md) if necessary.

